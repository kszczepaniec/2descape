#include "main.hh"
#include <gsl/gsl_histogram.h>
#include <fstream>



// tmp: /mnt/lustre/scratch/people/ufszczep/";
// storage: /storage/ufszczep/
char filePrefix[200] = "";


void makeGnuplotScript ( Settings& settings, double alpha, double fi, const char * data_fileName )
{
     char file[200];
     char plotfile[200];

     sprintf ( file,"%s/a_%2.3f_fi_%2.5f_escape_time.gnu",settings.getStoragePath(), alpha, fi );
     sprintf ( plotfile,"%s/a_%2.3f_fi_%2.5f_escape_time.png",settings.getStoragePath(), alpha, fi );
     ofstream gnu ( file,ios_base::out );
     gnu << "reset\n";

     gnu << "set terminal png enhanced size 1200,600;\n";
     gnu << "set output \""<<plotfile<<"\"\n";

//      gnu << "set xrange [0:25]\n";
//      gnu << "set yrange [0:25]\n";
     gnu << "set title '{/Symbol a} = "<<alpha<<" {/Symbol f} = "<< fi<<"'\n";
     gnu << "set xlabel 'escape time'\n";
     gnu << "plot '"<< data_fileName <<"' using ($1+ ($2-$1)/2):3 with linespoints title 'escape time';\n";


     gnu.close();
}


int main ( int argc, char **argv )
{
     cout << endl << endl;

     System& sys = System::getInstance();
     sys.start();

     sys.printInfo();

     Settings& settings = Settings:: getInstance ( "settings.dat" );
     settings.readCommandLineParameters ( argc,argv );
     settings.printParameters();


     Simulation * sim = new Simulation ( &settings );

     int nbins = settings.get ( "nbins" );
     double xmin = settings.get ( "xmin" );
     double xmax = settings.get ( "xmax" );


     double alfa = settings.getJumpsParameter();


     int nrepeats = settings.getNtrajectories();

     int tenP =1;
     if ( nrepeats>10 ) tenP = ( int ) ( nrepeats/10.0 );


     char datafile[200];
     sprintf ( datafile,"%s/a_%2.3f_data.txt",settings.getStoragePath(), alfa );
     ofstream output ( datafile,ios_base::out );

     double angle_start = M_PI/20.0;
     double angle_end = ( 3.0*M_PI ) /2.0;
     double angle_step = M_PI/20.0;

//      double alpha_start = 0.9;
//      double alpha_end = 2.0;
//      double alpha_step = 0.3;

     output << "#alpha" << "\tfi"<< "\tmfpt" << endl;
     char datafileName[200];



// for(double alfa = alpha_start; alfa <= alpha_end; alfa+=alpha_step)
// {
     for ( double angle = angle_start; angle <= angle_end; angle+=angle_step ) {

          double mfpt = 0.0;
          int count = 0;

//      settings.setJumpsParameter(alfa);


          gsl_histogram * histogram = gsl_histogram_alloc ( nbins );
          gsl_histogram_set_ranges_uniform ( histogram, xmin, xmax );



          for ( int i=0; i<nrepeats; i++ ) {
               if ( i % tenP ==0 ) cout << i << "/" << nrepeats<<endl;

               double t = sim->run ( angle );

               //cout << t <<endl;
               mfpt = mfpt + ( t - mfpt ) / ( ++count );

               gsl_histogram_increment ( histogram, t );
          }

          cout << "alpha = " << alfa <<"\tfi="<<angle<<"\t<t>="<<mfpt<<endl;

          output << alfa<<"\t"<<angle<<"\t"<<mfpt<<endl;



          sprintf ( datafileName,"a_%2.3f_fi_%2.5f_escape_time.txt", alfa, angle );

          makeGnuplotScript ( settings,  alfa, angle , datafileName );

          sprintf ( datafileName,"%s/a_%2.3f_fi_%2.5f_escape_time.txt",settings.getStoragePath(), alfa, angle );


          double scale = 1.0/ ( gsl_histogram_sum ( histogram ) );
          gsl_histogram_scale ( histogram,  scale );

          FILE * pFile = fopen ( datafileName,"w" );
          gsl_histogram_fprintf ( pFile, histogram , "%g", "%g" );
          fclose ( pFile );


          double histo_mfpt = gsl_histogram_mean ( histogram );

          cout << "histogram <t> = " << histo_mfpt <<endl;


          gsl_histogram_free ( histogram );


          makeGnuplotScript ( settings,  alfa, angle , datafileName );
     }
// }

     output.close();


     delete sim;

     sys.finish();
     sys.printTimeSummary();

     return 0;
}


