#include "main.hh"
#include <gsl/gsl_histogram.h>
#include <fstream>



// tmp: /mnt/lustre/scratch/people/ufszczep/";
// storage: /storage/ufszczep/
char filePrefix[200] = "";


void makeGnuplotScript ( Settings& settings, double alpha, double sigma , double fi,  const char * data_fileName )
{
     char file[200];
     char plotfile[200];

     sprintf ( file,"%s/a_%2.3f_s_%1.2f_fi_%2.5f_escape_time.gnu",settings.getStoragePath(), alpha, sigma, fi );
     sprintf ( plotfile,"a_%2.3f_s_%1.2f_fi_%2.5f_escape_time.png", alpha, sigma, fi );
     ofstream gnu ( file,ios_base::out );
     gnu << "reset\n";

     gnu << "set terminal png enhanced size 1200,600;\n";
     gnu << "set output \""<<plotfile<<"\"\n";

//      gnu << "set xrange [0:25]\n";
//      gnu << "set yrange [0:25]\n";
     gnu << "set title '{/Symbol a} = "<<alpha<<" {/Symbol s} = " << sigma << " {/Symbol f} = "<< fi<<"'\n";
     gnu << "set xlabel 'escape time'\n";
     gnu << "plot '"<< data_fileName <<"' using ($1+ ($2-$1)/2):3 with linespoints title 'escape time';\n";


     gnu.close();
}


int main ( int argc, char **argv )
{
     cout << endl << endl;

     System& sys = System::getInstance();
     sys.start();

     sys.printInfo();

     Settings& settings = Settings:: getInstance ( "settings.dat" );
     settings.readCommandLineParameters ( argc,argv );
     settings.printParameters();


     Simulation * sim = new Simulation ( &settings );

     int nbins = settings.get ( "nbins" );
     double xmin = settings.get ( "xmin" );
     double xmax = settings.get ( "xmax" );


     double alpha = settings.getJumpsParameter();
     double sigma = settings.getNoiseIntensity();


     double angle_start = settings.get ( "angle_start" ) *M_PI; //M_PI/20.0;
     double angle_end =  settings.get ( "angle_end" ) *M_PI;
     double angle_step = settings.get ( "angle_increment" ) *M_PI;

     double noiseType = settings.get( "NOISE_TYPE" );

     char datafile[200];
     sprintf ( datafile,"%s/a_%2.3f_s_%1.2f_data.txt",settings.getStoragePath(), alpha, sigma );
     ofstream output ( datafile,ios_base::out );



     output << "#alpha" << "\tsigma" << "\tfi"<< "\tmfpt" << "\tmfpt_err" << endl;
     char datafileName[200];

     
     
     DataSource * data = nullptr ;
     char dataFile[200];

     for ( double angle = angle_start; angle <= angle_end; angle+=angle_step ) {



          if ( settings.multipleOutputs() ) {
               sprintf ( dataFile,"%s/%s_alpha_%1.2f_s_%1.2f_fi_%2.5f_n%d_fpt_{NUM}.dat",settings.getDataPath(),settings.getFilesPrefix(), alpha, sigma ,angle , (int) noiseType );

               // multiple inputs, we will use chained input files
               int filesNum = settings.getMultipleOutputFilenum();  // use this number as max-number indicator

               cout << "opening "<< filesNum << "chain files "<< dataFile << endl;
               data = new DatafilesChain ( dataFile ,1, filesNum );
          } else {

               sprintf ( dataFile,"%s/%s_alpha_%1.2f_s_%1.2f_fi_%2.5f_fpt.dat",settings.getDataPath(), settings.getFilesPrefix(),alpha ,sigma ,angle );

               cout << "opening single file '"<< dataFile<< "'" <<endl;
               data = Datafile::open ( dataFile );
          }

          if ( data->ok() ) {
               cout << "datafile ok"<<endl;
          } else {
               cout << "datafile not ok, wrong angle?" <<endl;
               continue;
          }
          int count = data->getCount();
          cout << "datafile contains " << count << " values"<<endl;

	  if(count==0) {
          cout << " 0 values, skipping angle" << endl;
          continue;
	  }

          int tenPerc = ( int ) ( count/10.0 );


          double mfpt = 0.0;

          RunningStat * runningStat = new RunningStat();


          gsl_histogram * histogram = gsl_histogram_alloc ( nbins );
          gsl_histogram_set_ranges_uniform ( histogram, xmin, xmax );


          int c= 0;
          while ( data->hasNext() ) {
               if ( c % tenPerc ==0 ) cout << c << "/" << count<<endl;

               double t = data->read();
               //cout << t <<endl;
               mfpt = mfpt + ( t - mfpt ) / ( ++c );

               runningStat->Push ( t );

               gsl_histogram_increment ( histogram, t );

          }

          runningStat->Mean();
          double mfpt_err = runningStat->StandardDeviationOfMean();

          cout << "alpha = " << alpha <<"\t sigma="<<sigma<<"\t fi="<<angle<<"\t<t>="<<mfpt<<endl;
          cout << "mfpt2 = " << runningStat->Mean() <<  " \t +- " << mfpt_err << endl;

          output << alpha<<"\t"<<sigma <<"\t" << angle<<"\t"<<mfpt<< "\t" << mfpt_err << endl;


          delete runningStat;


          sprintf ( datafileName,"a_%2.3f_s_%1.2f_fi_%2.5f_escape_time.txt", alpha,sigma, angle );

          makeGnuplotScript ( settings,  alpha, sigma,  angle , datafileName );

          sprintf ( datafileName,"%s/a_%2.3f_s_%1.2f_fi_%2.5f_escape_time.txt",settings.getStoragePath(), alpha, sigma, angle );


          double scale = 1.0/ ( gsl_histogram_sum ( histogram ) );
          gsl_histogram_scale ( histogram,  scale );

          FILE * pFile = fopen ( datafileName,"w" );
          gsl_histogram_fprintf ( pFile, histogram , "%g", "%g" );
          fclose ( pFile );


          double histo_mfpt = gsl_histogram_mean ( histogram );

          cout << "histogram <t> = " << histo_mfpt <<endl;


          gsl_histogram_free ( histogram );


          makeGnuplotScript ( settings,  alpha, sigma,  angle , datafileName );
     }
// }

     output.close();


     delete sim;

     sys.finish();
     sys.printTimeSummary();

     return 0;
}


