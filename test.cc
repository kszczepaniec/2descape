 
#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;

bool onArc(double angle, double r, double x, double y) {
  if(angle > 0.0) {
    
    double fi_x = acos(x/r);
    double fi_y = asin(y/r);
    
    double at = atan2(y,x);
    if(y<0.0) {
     at = atan2(-y,-x) + M_PI; 
    }
    if(at <1.0e-6) at = 0.0;
    
     cout<< " fi_x = " << fi_x << "\t fi_y = "<<fi_y<<"\tat2="<<at<<endl;
     return at < angle;
  }
  else 
  {
   return false;
  }
}


int main() {
  
  
  
  double angle = M_PI/4.0;
  double r = 4.0;
  
  for( double fi = 0.0; fi <= 3*M_PI; fi+=M_PI/8.0) {
    
    double x = r*cos(fi);
    double y = r*sin(fi);
    
    
    cout<< " fi > 2 pi = " << ((fi > 2.0*M_PI)) << endl;
    cout<< " fi - pi = " << ((fi - 2.0*M_PI)) << endl;
    cout << ((int) (fi/M_PI) ) <<endl;
    
    cout<< " fi = " << ((fi > 2.0*M_PI) ? (fi - ( (int)( fi/M_PI ))*M_PI) : (fi)) << " ("<< setprecision(2)<<  (fi/M_PI)<< setprecision(5) << " pi)\t (x,y) = ("<<x<<","<<y<<")"<<endl;
    
    cout << "on arc: " << onArc( angle,  r,  x,  y);
    
    
    
    cout << endl;
    
  };
  
  
  
  
  
 return 0; 
}