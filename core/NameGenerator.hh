#ifndef __NAME_GENERATOR_HH_
#define __NAME_GENERATOR_HH_

#include "Settings.hh"
#include <string>
#include <sstream>


/**
 * Utility class that generates appropriate file names, pathes, etc., 
 * basing on current settings (parameters values) in order to
 * create uniquely-named files.
 * 
 * 
 * 
 */
using namespace std;

class NameGenerator {
  
  
private:
  static const char * getOutputFileExt(){ return "dat"; }
  
public:
  static string createMultipleOutputFilename(double angle);
  
  





};

#endif