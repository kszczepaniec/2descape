#include "NameGenerator.hh"
#include <set>




string NameGenerator::createMultipleOutputFilename(double angle)
{

     Settings& settings = Settings::getInstance();
     
     stringstream str;

     //sprintf ( dataFile,"_alpha_%1.2f_s_%1.2f_fi_%2.5f_fpt_%d.dat",, , alpha, sigma, angle, settings.getMultipleOutputFilenum() );

     str << settings.getStoragePath() << "/";


     str << settings.getFilesPrefix();
     str << "_alpha_" << setprecision(3) << fixed <<  settings.getJumpsParameter();
     str << "_s_" << setprecision(3) << settings.getNoiseIntensity();
     str << "_fi_" << setprecision(6) <<fixed <<  angle;
     
     
     str << "_fpt";
     str << "_" << settings.getMultipleOutputFilenum();

     str << "." << getOutputFileExt();

    return str.str();
}

