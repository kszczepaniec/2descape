#include "main.hh"
#include <gsl/gsl_histogram.h>
#include <fstream>



// tmp: /mnt/lustre/scratch/people/ufszczep/";
// storage: /storage/ufszczep/
char filePrefix[200] = "";



int main ( int argc, char **argv )
{
     cout << endl << endl;

     System& sys = System::getInstance();
     sys.start();

     sys.printInfo();

     Settings& settings = Settings:: getInstance ( "settings.dat" );
     settings.readCommandLineParameters ( argc,argv );
     settings.printParameters();

     double alpha = settings.getJumpsParameter();
     int nrepeats = settings.getNtrajectories();
     double sigma = settings.getNoiseIntensity();
     double angle_start = settings.get("angle_start")*M_PI;  //M_PI/20.0;
     double angle_end =  settings.get("angle_end")*M_PI;
     double angle_step = settings.get("angle_increment")*M_PI;
     
     double noiseType = settings.get( "NOISE_TYPE" );
     
     cout << "angle start = " << angle_start << "\n";
     cout << "angle end = " << angle_end << "\n";
     cout << "angle increment = " << angle_step << "\n";
     
     Simulation * sim = new Simulation ( &settings );

     char dataFile[200];
// sim->setDumpGnuplot(true);
// sim->setVerbose(true);

     int tenP =1;
     if ( nrepeats>10 ) tenP = ( int ) ( nrepeats/10.0 );



     
     


     for ( double angle = angle_start; angle <= angle_end; angle+=angle_step ) {

          if ( settings.multipleOutputs() ) {
               cout << " multiple outputs! generating file #" << settings.getMultipleOutputFilenum() <<endl;
               //wielokrotny output, wyjsciowy plik dat musi miec numerek przyslany z zewnatrz
               sprintf ( dataFile,"%s/%s_alpha_%1.2f_s_%1.2f_fi_%2.5f_n%d_fpt_%d.dat",settings.getStoragePath(), settings.getFilesPrefix(), alpha, sigma, angle, (int) noiseType, settings.getMultipleOutputFilenum() );

          } else {
               // wszystko do jednego pliku
               sprintf ( dataFile,"%s/%s_alpha_%1.2f_s_%1.2f_fi_%2.5f_fpt.dat",settings.getStoragePath(), settings.getFilesPrefix(), alpha , sigma, angle );
          }



          Datafile * data = Datafile::create ( dataFile );


          cout << "\n fi="<< setprecision ( 3 ) << ( angle/M_PI ) <<" pi "<<endl;

          for ( int i=0; i<nrepeats; i++ ) {
               if ( i % tenP ==0 ) cout << i << "/" << nrepeats<<endl;

               double t = sim->run ( angle );
	       
	       

               data->write ( t );

          }


          data->close();
     }





     delete sim;

     sys.finish();
     sys.printTimeSummary();

     return 0;
}


